int deplacementX, deplacementY;
int ballX, ballY;
int w, z, z2;
int rgbshift;
int colorRect;
int points;
int scoreL, scoreR;


void setup(){
  size(400,400);
  background(4,6,51);
  stroke(255);
  line(200,0,200,400);
   ballX = 200;
   ballY = 200; 
   deplacementX = 3;
   deplacementY = -2; 
   w = 15;
   z = 120;
   z2= 120;
   colorRect=0;
   rgbshift=1;
}


void draw(){
  design();
  move();
  checkCollisionScreen();
  checkCollisionPaddle();
  score();
  if (deplacementX == 0 && deplacementY == 0 && (scoreL < 5 && scoreR <5)){
    text("Press 'R' to restart" ,90,200);
  }
} 

void keyPressed() {
  println(keyCode);
  if (key == CODED) {
    if (keyCode == UP) {
      z2=z2-35;
    }
    else if (keyCode == DOWN) {
      z2=z2+35;
    } 
  }
    if (key == 'z' || key == 'Z'){
      z=z-35;
    }
    if (key == 's' || key == 'S') {
      z=z+35;
    }
    if (key == 'r' || key == 'R'){
    deplacementX = 3;
    deplacementY = -2;
  }
  if(keyCode == 32){
    scoreL = 0;
    scoreR = 0;
    z = 120;
    z2 = 120;
    ballToTheCenter();
  }
}

void move()
{
  ballX=ballX+deplacementX;
  ballY=ballY+deplacementY;
}
void gradiant(){
  colorRect=colorRect+rgbshift;
    
  if (colorRect == 255) {
    rgbshift=-1;
  }
  if (colorRect == 0) {
    rgbshift=1;
  }
}
 
void checkCollisionScreen (){
  
  if (ballY <= 0 && deplacementY < 0){
    deplacementY = -deplacementY;
  }
  if (ballY > height-10 && deplacementY > 0) 
  { 
    deplacementY = -deplacementY;
  }
}
  
void checkCollisionPaddle(){
   if (ballX < 50+15 && deplacementX < 0 && ballY>z && ballY<z+85){
    deplacementX = -deplacementX;}
   if (ballX > width-50 && deplacementX > 0 && ballY>z2 && ballY<z2+85){
   deplacementX = -deplacementX;}
}

void design (){
  background(4,6,51);
  stroke(255);
  line(200,0,200,400);
  fill(colorRect);
  rect(25,z,25,85);
  fill(colorRect,colorRect,255);
  stroke (255);
  smooth();
  ellipse(ballX,ballY,30,30);
  fill(colorRect);
  rect(width-50,z2,25,85);
  gradiant();
  texte();
  end();
}
void texte(){
  fill(255);
  textSize(20);
  text(scoreL,30,30);
  text(scoreR,width-30,30);
}

void score(){
     if (ballX < 10){
      scoreR = scoreR + 1;
      ballToTheCenter();
     }
    if (ballX > 400 ){
      scoreL = scoreL + 1;
      ballToTheCenter();
  }
}

void ballToTheCenter() {
  deplacementX = 0;
  deplacementY = 0;
  ballX =200;
  ballY = 200;
}

void end(){
  if(scoreR == 5){
      ballToTheCenter();
      println("GAME OVER");
      text("Player 2 won!",130,200);
  }
  if(scoreL == 5){
      ballToTheCenter();
      println("GAME OVER");
      text("Player 1 won!",130,200);
  }
 
}
